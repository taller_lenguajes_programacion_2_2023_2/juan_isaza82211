function checkLocalStorageItem(itemName) {
    // Check if the item exists in local storage
    if (localStorage.getItem(itemName)) {

        document.getElementById('login_text').innerHTML = "Bienvenido " + localStorage.getItem('nombre_cuenta');
        document.getElementById('registro_text').innerHTML = "Cerrar sesión";
    
        const anchor = document.getElementById('login_text');
        const clear = document.getElementById('registro_text');
        anchor.addEventListener('click', function (event) {
            event.preventDefault(); // Prevent the default action, such as navigating to a new page
        });
        anchor.style.cursor = 'text';
        anchor.title = 'Nombre completo: ' + localStorage.getItem('nom_completo_cuenta') + '    Correo: ' + localStorage.getItem('email_cuenta');

        clear.addEventListener('click', function () {

            clear.href = 'login.html';
            localStorage.removeItem('logged_true');
        });
    }
}